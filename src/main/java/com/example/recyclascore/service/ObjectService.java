package com.example.recyclascore.service;

import com.example.recyclascore.entity.Object;
import com.example.recyclascore.repository.ObjectRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.List;
import java.util.Optional;

    @Service
    public class ObjectService {

        private final ObjectRepository objectRepository;

        public ObjectService(ObjectRepository objectRepository) {
            this.objectRepository = objectRepository;
        }

        // Récupérer tous les objets
        public List<Object> getAllObjects() {
            return objectRepository.findAll();
        }

        // Récupérer un objet par son ID
        public Optional<Object> getObjectById(Long id) {
            return objectRepository.findById(id);
        }

        // Récupère l'indice de recyclabilité de l'objet
        public int getRecyclabilityIndexByName(String name) {
            Object object = objectRepository.findByName(name)
                    .orElseThrow(NoSuchElementException::new);
            return object.getRecyclabilityIndex();
        }

        // Créer un nouvel objet
        public Object createObject(Object object) {
            return objectRepository.save(object);
        }

        // Mettre à jour un objet
        public Optional<Object> updateObject(Long id, Object object) {
            // Utilisation d'Optional pour rendre explicite la possibilité d'une valeur absente
            return objectRepository.existsById(id) ? Optional.of(objectRepository.save(object)) : Optional.empty();
        }

        // Supprimer un objet
        public void deleteObject(Long id) {
            objectRepository.deleteById(id);
        }

    }

