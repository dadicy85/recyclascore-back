package com.example.recyclascore.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JwtService {

    private static final Logger logger = LoggerFactory.getLogger(JwtService.class);

    @Value("${jwt.secretKey}")
    private String tokenSecret;

    @Getter
    private final SecretKey secretKey;

    public JwtService(@Value("${jwt.secretKey}") String tokenSecret) {
        this.tokenSecret = tokenSecret;
        final byte[] secretKeyBytes = this.tokenSecret.getBytes();
        this.secretKey = Keys.hmacShaKeyFor(secretKeyBytes);
    }

    public String generateToken(String username, List<String> roles) {
        Instant now = Instant.now();
        Instant expiration = now.plusSeconds(3600); // expire après 1 heure
        Date expDate = Date.from(expiration);

        return Jwts.builder()
                .subject(username)
                .claim("roles", roles)
                .issuedAt(Date.from(now))
                .expiration(expDate)
                .signWith(secretKey)
                .compact();
    }

    public String getUsernameFromToken(String token) {
        Claims claims = Jwts.parser()
                .verifyWith(secretKey)
                .build()
                .parseSignedClaims(token)
                .getBody();

        return claims.getSubject();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token);
            return true;
        } catch (JwtException ex) {
            logger.error("Error validating JWT token: {}", ex.getMessage(), ex);
            return false;
        }
    }

    public Authentication getAuthentication(String token, List<String> roles) {
        String username = getUsernameFromToken(token);
        Collection<SimpleGrantedAuthority> authorities = roles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(username, null, authorities);
    }
}