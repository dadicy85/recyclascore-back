package com.example.recyclascore.config;

import com.example.recyclascore.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.List;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;

    @Autowired
    public JwtAuthenticationFilter(JwtService jwtService, UserDetailsService userDetailsService) {
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        // Extraire le jeton JWT de la requête
        String jwtToken = extractJwtToken(request);

        // Valider le jeton JWT et extraire les rôles s'il est valide
        if (StringUtils.hasText(jwtToken) && jwtService.validateToken(jwtToken)) {
            List<String> roles = extractRolesFromToken(jwtToken);

            // Créer une authentication à partir des rôles
            Authentication authentication = jwtService.getAuthentication(jwtToken, roles);

            // Définir l'authentication dans le contexte de sécurité
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        // Passer la requête au filtre suivant
        filterChain.doFilter(request, response);
    }

    private List<String> extractRolesFromToken(String token) {
        // Extraire les revendications du jeton
        Claims claims = Jwts.parser()
                .setSigningKey(jwtService.getSecretKey())
                .build()
                .parseClaimsJws(token)
                .getBody();

        // Extraire les rôles des revendications du jeton
        return claims.get("roles", List.class);
    }

    private String extractJwtToken(HttpServletRequest request) {
        // Extraire le jeton JWT de l'en-tête Authorization
        String bearerToken = request.getHeader("Authorization");

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }

        return null;
    }
}

