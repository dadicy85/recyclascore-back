package com.example.recyclascore.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "objects")
public class Object {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String material;
    private int recyclabilityIndex;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}