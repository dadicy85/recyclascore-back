package com.example.recyclascore.repository;

import com.example.recyclascore.entity.Object;
import com.example.recyclascore.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ObjectRepository extends JpaRepository<Object, Long> {
    Optional<Object> getObjectById(Long id);

    Optional<Object> findByName(String name);
}

