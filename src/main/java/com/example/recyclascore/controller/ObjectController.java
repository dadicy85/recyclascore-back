package com.example.recyclascore.controller;


import com.example.recyclascore.entity.Object;
import com.example.recyclascore.entity.User;
import com.example.recyclascore.service.ObjectService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/api/objects")
@CrossOrigin(origins = "http://localhost:4200")
public class ObjectController {

    private final ObjectService objectService;

    public ObjectController(ObjectService objectService) {
        this.objectService = objectService;
    }

    // Récupère un objet par son ID
    @GetMapping("/{id}")
    public Optional<Object> getObjectById(@PathVariable Long id) {
        return objectService.getObjectById(id);
    }

    // Récupère tous les objets
    @GetMapping("getObjects")
    public ResponseEntity<List<Object>> getAllObjects() {
        List<Object> objects = objectService.getAllObjects();
        return ResponseEntity.ok(objects);
    }

    // Récupère l'indice de recyclabilité de l'objet
    @GetMapping("/recyclabilityIndex")
    public ResponseEntity<Integer> getRecyclabilityIndex(@RequestParam String name) {
        try {
            int recyclabilityIndex = objectService.getRecyclabilityIndexByName(name);
            return ResponseEntity.ok(recyclabilityIndex);
        } catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }
    }

    // Création d'un objet
    @PostMapping
    public Object createObject(@RequestBody Object object) {
        return objectService.createObject(object);
    }

    // Mise à jour d'un objet
    @PutMapping("/{id}")
    public Optional<Object> updateObject(@PathVariable Long id, @RequestBody Object object) {
        return objectService.updateObject(id, object);
    }

    // Supprimer un objet
    @DeleteMapping("/{id}")
    public void deleteObject(@PathVariable Long id) {
        objectService.deleteObject(id);
    }
}
