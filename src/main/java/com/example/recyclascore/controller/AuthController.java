package com.example.recyclascore.controller;

import com.example.recyclascore.dto.AuthenticationResponse;
import com.example.recyclascore.entity.User;
import com.example.recyclascore.service.JwtService;
import com.example.recyclascore.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "http://localhost:4200")
public class AuthController {

    private final UserService userService;
    private final JwtService jwtService;

    public AuthController(AuthenticationManager authenticationManager, UserService userService, JwtService jwtService) {
        this.userService = userService;
        this.jwtService = jwtService;
    }

    // Inscription
    @PostMapping("/register")
    public ResponseEntity<Void> createUser(@RequestBody User user) {
        Optional<User> createdUser = Optional.ofNullable(userService.registerUser(user));
        if (createdUser.isPresent()) {
            URI location = URI.create("/api/auth/" + createdUser.get().getId());
            return ResponseEntity.created(location).build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    // Connexion
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> connectUser(@RequestBody User user) {
        Optional<User> connectedUser = userService.connectUser(user.getEmail(), user.getPassword());
        if (connectedUser.isPresent()) {
            // Génère le token JWT
            String userName = connectedUser.get().getUsername();
            List<String> roles = userService.getUserRoles(connectedUser);

            String token = jwtService.generateToken(userName, roles);
            AuthenticationResponse authenticationResponse = new AuthenticationResponse();
            authenticationResponse.setToken(token);
            return ResponseEntity.ok(authenticationResponse);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    // Déconnexion
    @PostMapping("/logout")
    public ResponseEntity<Void> logoutUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return ResponseEntity.ok().build();
    }
}
